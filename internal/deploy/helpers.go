package deploy

import (
	"encoding/base64"
	"fmt"
	log "github.com/sirupsen/logrus"
	"os"
	"strconv"
	"strings"
)

func GetDeployName(track, releaseName string) string {
	if track != TrackStable {
		return fmt.Sprintf("%s-%s", releaseName, track)
	}
	return releaseName
}

func GetReplicas(track string) int {
	envTrack := strings.ToUpper(track)
	envSlug := strings.ToUpper(strings.ReplaceAll(os.Getenv(EnvSlug), "-", "_"))

	envTrackReplicas := os.Getenv(fmt.Sprintf("%s_%s_%s", envTrack, envSlug, Replicas))
	envReplicas := os.Getenv(fmt.Sprintf("%s_%s", envSlug, Replicas))
	var newReplicas string
	if envTrackReplicas != "" {
		newReplicas = envTrackReplicas
	}
	if envReplicas != "" {
		newReplicas = envReplicas
	}
	if replicas := os.Getenv(Replicas); replicas != "" {
		newReplicas = replicas
	}
	// if nothing requested, return 1
	if newReplicas == "" {
		return 1
	}
	val, err := strconv.Atoi(newReplicas)
	if err != nil {
		log.WithError(err).Errorf("unable to parse replica value: '%s'", newReplicas)
		return 1
	}
	return val
}

func GetIPS() string {
	if os.Getenv(ProjectVisibility) == ProjectVisibilityPublic {
		log.Info("skipping ImagePullSecret as this is a public repo")
		return ""
	}
	ipsUsername := os.Getenv(RegistryUser)
	ipsPassword := os.Getenv(RegistryPassword)
	if username := os.Getenv(DeployUser); username != "" {
		log.Infof("using deploy token (%s) for registry auth...", username)
		ipsUsername = username
		ipsPassword = os.Getenv(DeployPassword)
	} else {
		log.Warning("using temporary registry token for registry auth - this will cause ImagePullBackOff problems unless you've configured authentication elsewhere")
	}

	return fmt.Sprintf(`{"auths":{"%s":{"username":"%s","password":"%s","email":"%s","auth":"%s"}}}`,
		os.Getenv(Registry),
		ipsUsername,
		ipsPassword,
		os.Getenv(GitLabUserEmail),
		base64.URLEncoding.EncodeToString([]byte(fmt.Sprintf("%s:%s", ipsUsername, ipsPassword))),
	)
}

func GetAdditionalHosts() []string {
	envSlug := strings.ToUpper(strings.ReplaceAll(os.Getenv(EnvSlug), "-", "_"))
	envHosts := os.Getenv(fmt.Sprintf("%s_%s", envSlug, AdditionalHosts))
	if envHosts != "" {
		return strings.Split(envHosts, ",")
	}
	hosts := os.Getenv(AdditionalHosts)
	if hosts == "" {
		return nil
	}
	return strings.Split(hosts, ",")
}
