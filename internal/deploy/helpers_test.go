package deploy

import (
	"github.com/stretchr/testify/assert"
	"os"
	"testing"
)

func TestGetDeployName(t *testing.T) {
	assert.EqualValues(t, "production-93", GetDeployName(TrackStable, "production-93"))
	assert.EqualValues(t, "production-93-canary", GetDeployName("canary", "production-93"))
}

func TestGetReplicas(t *testing.T) {
	var cases = []struct {
		name             string
		trackEnvReplicas string
		envReplicas      string
		replicas         string
		expected         int
	}{
		{
			"track replica is detected",
			"23",
			"",
			"",
			23,
		},
		{
			"env replica is detected",
			"23",
			"245",
			"",
			245,
		},
		{
			"replicas is detected",
			"23",
			"245",
			"1234",
			1234,
		},
		{
			"no values return 1",
			"",
			"",
			"",
			1,
		},
		{
			"not a number returns 1",
			"",
			"",
			"three",
			1,
		},
	}

	for _, tt := range cases {
		t.Run(tt.name, func(t *testing.T) {
			assert.NoError(t, os.Setenv("STABLE_PRODUCTION_REPLICAS", tt.trackEnvReplicas))
			assert.NoError(t, os.Setenv("PRODUCTION_REPLICAS", tt.envReplicas))
			assert.NoError(t, os.Setenv(Replicas, tt.replicas))
			assert.NoError(t, os.Setenv(EnvSlug, "production"))
			assert.EqualValues(t, tt.expected, GetReplicas(TrackStable))
		})
	}
}

func TestGetIPS(t *testing.T) {
	var cases = []struct {
		name       string
		regUser    string
		regPass    string
		depUser    string
		depPass    string
		visibility string
		expected   string
	}{
		{
			"default is to use the job token",
			"gitlab-ci-token",
			"hunter2",
			"",
			"",
			"internal",
			`{"auths":{"registry.gitlab.com":{"username":"gitlab-ci-token","password":"hunter2","email":"joe@example.org","auth":"Z2l0bGFiLWNpLXRva2VuOmh1bnRlcjI="}}}`,
		},
		{
			"public project returns nothing",
			"gitlab-ci-token",
			"hunter2",
			"",
			"",
			"public",
			"",
		},
		{
			"deploy token is preferred",
			"gitlab-ci-token",
			"hunter2",
			"gitlab-deploy-token-123",
			"password",
			"internal",
			`{"auths":{"registry.gitlab.com":{"username":"gitlab-deploy-token-123","password":"password","email":"joe@example.org","auth":"Z2l0bGFiLWRlcGxveS10b2tlbi0xMjM6cGFzc3dvcmQ="}}}`,
		},
	}

	for _, tt := range cases {
		t.Run(tt.name, func(t *testing.T) {
			assert.NoError(t, os.Setenv(ProjectVisibility, tt.visibility))
			assert.NoError(t, os.Setenv(RegistryUser, tt.regUser))
			assert.NoError(t, os.Setenv(RegistryPassword, tt.regPass))
			assert.NoError(t, os.Setenv(DeployUser, tt.depUser))
			assert.NoError(t, os.Setenv(DeployPassword, tt.depPass))
			assert.NoError(t, os.Setenv(Registry, "registry.gitlab.com"))
			assert.NoError(t, os.Setenv(GitLabUserEmail, "joe@example.org"))
			assert.EqualValues(t, tt.expected, GetIPS())
		})
	}
}

func TestGetAdditionalHosts(t *testing.T) {
	var cases = []struct {
		name     string
		envHosts string
		hosts    string
		expected []string
	}{
		{
			"env hosts are preferred",
			"https://example.org",
			"https://example.com",
			[]string{"https://example.org"},
		},
		{
			"no env hosts use fallback",
			"",
			"https://example.com",
			[]string{"https://example.com"},
		},
	}
	for _, tt := range cases {
		t.Run(tt.name, func(t *testing.T) {
			assert.NoError(t, os.Setenv(EnvSlug, "production"))
			assert.NoError(t, os.Setenv("PRODUCTION_ADDITIONAL_HOSTS", tt.envHosts))
			assert.NoError(t, os.Setenv(AdditionalHosts, tt.hosts))
			assert.ElementsMatch(t, tt.expected, GetAdditionalHosts())
		})
	}
}
