package deploy

// environment variables provided by GitLab
//goland:noinspection GoNameStartsWithPackageName
const (
	ProjectVisibility = "CI_PROJECT_VISIBILITY"
	Registry          = "CI_REGISTRY"
	RegistryUser      = "CI_REGISTRY_USER"
	RegistryPassword  = "CI_REGISTRY_PASSWORD"
	DeployUser        = "CI_DEPLOY_USER"
	DeployPassword    = "CI_DEPLOY_PASSWORD"
	GitLabUserEmail   = "GITLAB_USER_EMAIL"
	Replicas          = "REPLICAS"
	EnvSlug           = "CI_ENVIRONMENT_SLUG"
	AdditionalHosts   = "ADDITIONAL_HOSTS"
)

// values that are likely to be set by
// a user
const (
	ProjectVisibilityPublic = "public"
	TrackStable             = "stable"
)
