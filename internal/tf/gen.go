package tf

import (
	"bytes"
	_ "embed"
	"github.com/Masterminds/sprig"
	"github.com/miracl/conflate"
	log "github.com/sirupsen/logrus"
	"os"
	"strings"
	"text/template"
)

//go:embed main.tf
var tfTemplate string

//go:embed application.goyaml
var k8sTemplate string

//go:embed values.yaml
var valuesTemplate string

const (
	FormatTF  = "tf"
	FormatK8S = "k8s"
)

type ProjectConfig struct {
	ServerAddr  string
	ProjectName string
	Chart       ChartConfig
	Deploy      DeployConfig
	App         AppConfig
	Kube        KubeConfig
	Env         map[string]string
}

type ChartConfig struct {
	URL    string
	Name   string
	Values string
}

type AppConfig struct {
	Name string
}

type DeployConfig struct {
	Track           string
	AdditionalHosts []string
	ReplicaCount    int
	CanaryWeight    int
	IPS             string
}

type KubeConfig struct {
	Name      string
	Namespace string
}

func New(c *ProjectConfig, format string) (string, error) {
	values, err := parseTemplate(c, valuesTemplate)
	if err != nil {
		return "", err
	}
	// merge the values that the user provider with our own
	conf, err := conflate.FromData([]byte(c.Chart.Values), []byte(values))
	if err != nil {
		log.WithError(err).Error("failed to merge values")
		return "", err
	}
	data, err := conf.MarshalYAML()
	if err != nil {
		log.WithError(err).Error("failed to marshal merged yaml")
		return "", err
	}
	c.Chart.Values = string(data)
	var tpl string
	switch format {
	case FormatK8S:
		tpl = k8sTemplate
	case FormatTF:
		fallthrough
	default:
		tpl = tfTemplate
	}
	return parseTemplate(c, tpl)
}

func parseTemplate(c *ProjectConfig, tpl string) (string, error) {
	// pull all environment variables
	envMap := make(map[string]string)
	for _, v := range os.Environ() {
		splitV := strings.Split(v, "=")
		envMap[splitV[0]] = splitV[1]
	}

	tmpl := template.New("values").Funcs(sprig.TxtFuncMap())
	tmpl, err := tmpl.Parse(tpl)
	if err != nil {
		log.WithError(err).Error("failed to parse template")
		return "", err
	}
	var buf bytes.Buffer
	c.Env = envMap
	err = tmpl.Execute(&buf, c)
	if err != nil {
		log.WithError(err).Error("failed to merge template")
		return "", err
	}
	return buf.String(), nil
}
