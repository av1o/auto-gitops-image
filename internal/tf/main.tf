terraform {
  backend "http" {
    lock_method = "POST"
    unlock_method = "DELETE"
    retry_wait_min = 5
  }
  required_providers {
    argocd = {
      source = "oboukili/argocd"
      version = "1.2.1"
    }
  }
}

provider "argocd" {
  server_addr = "{{ .ServerAddr }}"
  grpc_web = true
}

resource "argocd_application" "olm" {
  metadata {
    name = "{{ printf "%.63s" .App.Name }}"
  }
  wait = false
  spec {
    project = "{{ .ProjectName }}"
    source {
      repo_url = "{{ .Chart.URL }}"
      chart = "{{ .Chart.Name }}"
      target_revision = "*"
      helm {
        release_name = "{{ printf "%.63s" .App.Name }}"
        parameter {
          {{- if eq .Env.CI_PROJECT_VISIBILITY "public" }}
          name  = "image.secrets"
          value = "null"
          {{- else }}
          name = "image.secrets[0].name"
          value = "gitlab-registry-{{ .App.Name }}"
          {{- end }}
        }
        values = <<EOT
{{ .Chart.Values }}
EOT
      }
    }
    destination {
      name = "{{ .Kube.Name }}"
      namespace = "{{ .Kube.Namespace }}"
    }
    sync_policy {
      automated = {
        prune = true
        self_heal = true
      }
    }
  }
}