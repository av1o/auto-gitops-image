package tf

import (
	_ "embed"
	"github.com/stretchr/testify/assert"
	"io/ioutil"
	"os"
	"testing"
)

func TestNew(t *testing.T) {
	assert.NoError(t, os.Setenv("CI_PROJECT_PATH_SLUG", "group-project"))
	assert.NoError(t, os.Setenv("CI_ENVIRONMENT_SLUG", "production"))
	assert.NoError(t, os.Setenv("CI_ENVIRONMENT_NAME", "production"))
	assert.NoError(t, os.Setenv("CI_ENVIRONMENT_URL", "https://production.example.org"))
	assert.NoError(t, os.Setenv("CI_PROJECT_VISIBILITY", "internal"))
	assert.NoError(t, os.Setenv("CI_REGISTRY_IMAGE", "registry.gitlab.example.com/group/project"))
	assert.NoError(t, os.Setenv("CI_APPLICATION_TAG", "v2.0"))

	data, _ := ioutil.ReadFile("./testdata/values.yaml")

	config := &ProjectConfig{
		ServerAddr:  "argocd.example.com:443",
		ProjectName: "default",
		Chart: ChartConfig{
			Values: string(data),
		},
		Deploy: DeployConfig{
			Track:           "stable",
			AdditionalHosts: []string{"https://production-us.example.org", "https://production-us-east.example.org"},
			ReplicaCount:    2,
			CanaryWeight:    0,
			IPS:             `{"auths": {}}`,
		},
		App: AppConfig{
			Name: "production-123",
		},
		Kube: KubeConfig{
			Name:      "localhost",
			Namespace: "default",
		},
	}
	t.Run("terraform formatter", func(t *testing.T) {
		str, err := New(config, FormatTF)
		assert.NoError(t, err)
		t.Log(str)
	})
	t.Run("k8s formatter", func(t *testing.T) {
		str, err := New(config, FormatK8S)
		assert.NoError(t, err)
		t.Log(str)
	})
}

func TestNew_NoURL(t *testing.T) {
	assert.NoError(t, os.Setenv("CI_PROJECT_PATH_SLUG", "group-project"))
	assert.NoError(t, os.Setenv("CI_ENVIRONMENT_SLUG", "production"))
	assert.NoError(t, os.Setenv("CI_ENVIRONMENT_NAME", "production"))
	assert.NoError(t, os.Setenv("CI_ENVIRONMENT_URL", "https://production.example.org"))
	assert.NoError(t, os.Setenv("ADDITIONAL_HOSTS", ""))
	assert.NoError(t, os.Setenv("CI_PROJECT_VISIBILITY", "internal"))
	assert.NoError(t, os.Setenv("CI_REGISTRY_IMAGE", "registry.gitlab.example.com/group/project"))
	assert.NoError(t, os.Setenv("CI_APPLICATION_TAG", "v2.0"))

	data, _ := ioutil.ReadFile("./testdata/values.yaml")

	config := &ProjectConfig{
		ServerAddr:  "argocd.example.com:443",
		ProjectName: "default",
		Chart: ChartConfig{
			Values: string(data),
		},
		Deploy: DeployConfig{
			Track:           "stable",
			AdditionalHosts: nil,
			ReplicaCount:    2,
			CanaryWeight:    0,
			IPS:             `{"auths": {}}`,
		},
		App: AppConfig{
			Name: "production-123",
		},
		Kube: KubeConfig{
			Name:      "localhost",
			Namespace: "default",
		},
	}
	t.Run("terraform formatter", func(t *testing.T) {
		str, err := New(config, FormatTF)
		assert.NoError(t, err)
		t.Log(str)
	})
}
