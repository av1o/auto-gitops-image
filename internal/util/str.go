package util

// TruncateString limits the length of a string
// to a set limit.
func TruncateString(str string, length int) string {
	if len(str) > length {
		return str[0:length]
	}
	return str
}
