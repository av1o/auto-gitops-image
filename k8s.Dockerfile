ARG GITLAB_PREFIX
FROM ${GITLAB_PREFIX}registry.gitlab.com/av1o/base-images/go-git:1.17 as BUILDER

RUN mkdir -p /home/somebody/go && \
    mkdir -p /home/somebody/.tmp
WORKDIR /home/somebody/go

ARG GOPRIVATE
ARG AUTO_DEVOPS_GO_COMPILE_FLAGS

# copy our code in
COPY --chown=somebody:0 . .

# build the binary
RUN TMPDIR=/home/somebody/.tmp CGO_ENABLED=0 GOOS=linux GOARCH=amd64 \
    go build -a -installsuffix cgo -ldflags '-extldflags "-static"' $(echo "${AUTO_DEVOPS_GO_COMPILE_FLAGS}" | tr -d '"' | tr -d "'" | sed -e 's/%20/ /g') -o gitops ./cmd/gitops

FROM registry.gitlab.com/av1o/auto-deploy-image:v3-1.18

COPY --from=BUILDER --chown=somebody:0 /home/somebody/go/gitops /build/bin/gitops

# set permissions
RUN chmod +x /build/bin/gitops
USER somebody