# auto-gitops-image

The [AutoGitOps](https://gitlab.com/av1o/gitlab-ci-templates/-/blob/master/deploy/AutoGitOps.gitlab-ci.yml) image.

## Overview

This project provides a binary which generates the required IaC to deploy an AutoDeployApp using ArgoCD.
No ArgoCD manifests get committed, they are generated and templated on-the-fly by this application.

## Configuration

All configuration gets pulled from environment variables provided by GitLab.

Some values must be set:

* `GITOPS_ARGO_ADDR` - address of the ArgoCD instance (e.g. `argocd.example.org:443`)
* `GITOPS_ARGO_PROJECT` - name of the ArgoCD project (default: `default`)
* `GITOPS_ARGO_NAMESPACE` - Kubernetes namespace that ArgoCD resides in (only required when using `k8s` output)
* `KUBE_CLUSTER_NAME` - name of the ArgoCD Kubernetes cluster to deploy to
* `ARGOCD_AUTH_TOKEN` - API key that can access ArgoCD

Additional configuration can be found by viewing the flags in [main.go](./cmd/gitops/main.go)