package main

import (
	"fmt"
	"github.com/namsral/flag"
	log "github.com/sirupsen/logrus"
	"gitlab.com/av1o/auto-gitops-image/internal/deploy"
	"gitlab.com/av1o/auto-gitops-image/internal/tf"
	"gitlab.com/av1o/auto-gitops-image/internal/util"
	"io/ioutil"
	"os"
)

func main() {
	log.SetOutput(os.Stderr)

	argoAddr := flag.String("gitops-argo-addr", "", "address of the ArgoCD instance in the host:port format (e.g. argocd.example.org:443)")
	argoProject := flag.String("gitops-argo-project", "default", "ArgoCD project to deploy into (default: default)")

	appName := flag.String("helm-release-name", "", "name of the deployed helm release")

	kubeClusterName := flag.String("kube-cluster-name", "https://kubernetes.default.svc", "URL to the Kubernetes cluster")
	kubeNamespace := flag.String("kube-namespace", "", "kubernetes namespace to deploy into")

	chartRepo := flag.String("auto-devops-chart-repository", "https://av1o.gitlab.io/charts", "helm chart repo to fetch the chart from.")
	chartName := flag.String("auto-devops-chart", "auto-deploy-app", "helm chart name to retrieve")
	chartValues := flag.String("helm-upgrade-values-file", ".gitlab/auto-deploy-values.yaml", "helm values file to read")

	track := flag.String("track", "stable", "deploy track")
	percentage := flag.Int("percentage", 100, "deploy rollout percentage")

	formatMode := flag.String("format", tf.FormatTF, "output format (tf, k8s)")

	flag.Parse()

	// generate 'dynamic' variables
	var releaseName = *appName
	if releaseName == "" {
		releaseName = fmt.Sprintf("%s-%s", os.Getenv("CI_ENVIRONMENT_SLUG"), os.Getenv("CI_PROJECT_ID"))
	}
	releaseName = util.TruncateString(releaseName, 63)

	// read the helm values
	values, err := ioutil.ReadFile(*chartValues)
	if err != nil {
		log.WithError(err).Errorf("failed to read values at path: '%s'", *chartValues)
	}

	projectConfig := &tf.ProjectConfig{
		ServerAddr:  *argoAddr,
		ProjectName: *argoProject,
		Chart: tf.ChartConfig{
			URL:    *chartRepo,
			Name:   *chartName,
			Values: string(values),
		},
		App: tf.AppConfig{
			Name: deploy.GetDeployName(*track, releaseName),
		},
		Kube: tf.KubeConfig{
			Name:      *kubeClusterName,
			Namespace: *kubeNamespace,
		},
		Deploy: tf.DeployConfig{
			Track:           *track,
			AdditionalHosts: deploy.GetAdditionalHosts(),
			ReplicaCount:    deploy.GetReplicas(*track),
			CanaryWeight:    *percentage,
			IPS:             deploy.GetIPS(),
		},
	}
	str, err := tf.New(projectConfig, *formatMode)
	if err != nil {
		log.WithError(err).Fatal("couldn't setup template data")
		return
	}
	fmt.Print(str)
}
